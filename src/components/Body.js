import React from 'react';
import './Body.scss';
import axios from 'axios';

export default class Body extends React.Component{
 
  state={
    rubriker:["Lorem ipsum dolor sit amet, consectetur", "Morbi accumsan pharetra sapien, in semper", "Nullam hendrerit eleifend tortor, sit amet"],
    text:[ "Lorem ipsum dolor sit amet, consectetur adipiscing elit. Morbi gravida dolor", "Lorem ipsum dolor sit amet, consectetur adipiscing elit. Morbi gravida dolor.", "Lorem ipsum dolor sit amet, consectetur adipiscing elit. Morbi gravida dolor."],
    active: false
  }
 
  fetchData = ()=>{
    axios.get('http://localhost:3001/api/text')
    .then(res => {
        const texter = res.data;
        console.log(texter);
        this.setState({ 
          rubriker: texter.rubrik,
          text: texter.text  
      });
    })
  }

 render() {

  return(
      <div className="Body">
        
        <div className="puff" tabIndex="1">
          <img src={require('./img/writing.jpg')} alt="writing"/>
            <p className="imgText">Utveckling</p>
              <h1> {this.state.rubriker[0]} </h1>
              <p className="divText"> {this.state.text[0]} </p>
        </div>
  
        <div className="puff" tabIndex="2">
          <img src={require('./img/office.jpg')} alt="office"/>
            <p className="imgText">Utveckling</p>
              <h1>  {this.state.rubriker[1]} </h1>
              <p className="divText"> {this.state.text[1]} </p>
        </div>

        <div className="puff" tabIndex="3">
          <img src={require('./img/home-office.jpg')} alt="home-office"/>
            <p className="imgText">Utveckling</p>
              <h1>  {this.state.rubriker[2]}  </h1>
              <p className="divText"> {this.state.text[2]} </p>
        </div>
      <button className="button" onClick={this.fetchData=this.fetchData.bind(this)}> Hämta in nya puffar </button>
    </div>
      );
    }
  }