const express = require('express');
const fs = require('fs');
const app = express(),
cors = require('cors');

let text = fs.readFileSync(__dirname + '/text.json');
let newText = JSON.parse(text);

app.use(cors());

app.get('/api/text', (req, res) => {
    res.json(newText);
  });

app.listen(3001, () =>
  console.log('Express server is running on localhost:3001')
);